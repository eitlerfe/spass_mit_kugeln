import numpy as np
import scipy.optimize
from scipy.special import gamma, polygamma
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt


#derivative of volume function with respect dimension
def f(n):
    return np.pi**(n/2) * (np.log(np.pi) - polygamma(0, n/2+1))/(2*gamma(n/2+1))
#volume function
def vol(n):
    return np.pi**(n/2) / gamma(n/2+1)

#solving using fsolve
#x_0 start iteration
x_0 = 7
n = scipy.optimize.fsolve(f, x_0)
print(n, vol(n))
#try also different solvers
x1 = scipy.optimize.broyden1(f, x_0)
x2 = scipy.optimize.broyden2(f, x_0)
x3 = scipy.optimize.anderson(f, x_0)
x4 = scipy.optimize.newton_krylov(f, x_0)
print(x1, x2, x3, x4)
#=================================================

y = np.linspace(-1, 15, 100)
plt.plot(y, vol(y), 'green', label='Volumen($n$)')
plt.plot(y, f(y), 'r', label='Ableitung Volumen($n$)')
plt.legend(loc='upper right')
plt.grid()
plt.xlabel('Dimension $n$')
plt.savefig('Plot_volume.png', dpi=300)
plt.show()

