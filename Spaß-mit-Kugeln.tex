\documentclass{zirkel}
%-----------------------------------------------------------------------------
\usepackage{tikz}
\usepackage{dsfont}	
\usepackage{wasysym}
\usepackage{ccicons}
\usepackage{mathrsfs}
\usepackage[a4paper, lmargin={2.5cm}, rmargin={2.5cm}, tmargin={4cm}, bmargin=3cm]{geometry}
\usetikzlibrary{patterns}
\usepackage{listings}
\lstset{numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstset{language=Python}
\usepackage{pythonhighlight}
%-----------------------------------------------------------------------------
\renewcommand{\R}{\mathds{R}}
\renewcommand{\N}{\mathds{N}}
\newcommand{\dist}[2]{\mathrm{dist}(#1,#2)}
\renewcommand{\labelenumi}{\roman{enumi})}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\dx}[1]{ \,\mathrm{d}#1}
%-----------------------------------------------------------------------------
\newtheorem{defi}{Definition}
\newtheorem*{bem}{Bemerkung}
%---------------------------------------------------------------------------
\def\slfrac#1#2{{\mathord{\mathchoice   % Bruch mit schraegem Bruchstrich
			{\kern.1em\raise.5ex\hbox{$\scriptstyle#1$}\kern-.1em
				/\kern-.15em\lower.25ex\hbox{$\scriptstyle#2$}}
			{\kern.1em\raise.5ex\hbox{$\scriptstyle#1$}\kern-.1em
				/\kern-.15em\lower.25ex\hbox{$\scriptstyle#2$}}
			{\kern.1em\raise.4ex\hbox{$\scriptscriptstyle#1$}\kern-.1em
				/\kern-.14em\lower.25ex\hbox{$\scriptscriptstyle#2$}}
			{\kern.1em\raise.2ex\hbox{$\scriptscriptstyle#1$}\kern-.1em
				/\kern-.1em\lower.25ex\hbox{$\scriptscriptstyle#2$}}}}}
%-----------------------------------------------------------------------------
\renewcommand{\schuljahr}{2020/21}
\renewcommand{\klasse}{11/12}
\renewcommand{\datum}{\today} %Wann der Brief verschickt wird
\renewcommand{\titel}{Spaß mit Kugeln\footnote{ Spaß mit Kugeln by Ferdinand Eitler is licensed under CC BY-NC 4.0 \ccLogo\ccAttribution\ccNonCommercial}}
\renewcommand{\name}{Ferdinand Eitler }
%=============================================================================
%==========================end===preamble=====================================
%=============================================================================
\begin{document}

\makeheader

\noindent Frei nach Sheldons \glqq Spaß mit Flaggen\grqq{} steht der diesmalige Zirkelbrief unter dem Titel \glqq Spaß mit Kugeln\grqq{}, denn genau wie Flaggen sind Kugeln auch sehr cool. Wir schauen uns die folgenden Fragen etwas genauer an: 
\begin{itemize}
	\item Was ist eine Kugel überhaupt genau?
	\item Müssen Kugeln immer \glqq rund\grqq{} sein?
	\item Was sind höherdimensionale Kugeln? 
	\item In welcher Dimension hat die Kugel ein maximales Volumen? 
\end{itemize}

%----------------------------------------------------------------------------
\vspace{0.2cm}
\noindent\textbf{Einleitung und Motivation:} Jeder kennt aus der Schule oder aus dem täglichem Leben den Begriff der Kugel beispielsweise vom Fußballspielen, Eis essen oder von Seifenblasen. Normalerweise stellen wir uns dabei ein Objekt wie dieses vor: \\
\begin{figure}[ht]
	\begin{center}
	\begin{tikzpicture}
	\shade[ball color = gray!40, opacity = 0.4] (0,0) circle (2cm);
	\draw (0,0) circle (2cm);
	\draw (-2,0) arc (180:360:2 and 0.6);
	\draw[dashed] (2,0) arc (0:180:2 and 0.6);
	\fill[fill=black] (0,0) circle (1pt);
	\draw[dashed] (0,0 ) -- node[above]{$r$} (2,0);
	\draw (0,0) node[above]{$\vec{M}$};
	\end{tikzpicture}
	\end{center}
	\caption{Die Standardkugel im $\R^3$}
\end{figure}


Um eine Kugel zu beschreiben müssen wir nur den Mittelpunkt $\vec{M}=(x,y,z)\in\R^3$ und den Radius $r$,  quasi einen Punkt und den \textit{Abstand} zu diesem Punkt, wissen. Dies führt uns schon zu unserer ersten Frage: \\

\textbf{Was ist eine Kugel genau?}
Um eine allgemeine Kugel mathematisch zu beschreiben wollen wir nichts weiter als einen Punkt und einen \textit{Abstandsbegriff} verwenden. 
%-----------------------------------------------------------------------------
\begin{aufgabe}
	Überlege dir (ohne auf der nächsten Seit zu spicken \smiley ) welche Eigenschaften eine Abstandsfunktion $\dist{x}{y}$, d.h. eine Funktion die zwei Punkten $x$ und $y$ deren Abstand zuordnet, sinnvollerweise erfüllen sollte. 
\end{aufgabe}
%----------------------------------------------------------------------------
\begin{defi}
	Sei $X$ eine Menge. Eine Funktion $\mathrm{dist}: X\times X\to \R$ heißt \emph{Abstand} oder \emph{Metrik} auf $X$ falls für alle $x,y,z\in X$ gilt:
	\begin{enumerate}
		\item $\dist{x}{y}\geq 0$, 
		\item $\dist{x}{y}=0$ genau dann, wenn $x=y$,
		\item $\dist{x}{y}=\dist{y}{x}$, 
		\item $\dist{x}{z}\leq \dist{x}{y}+\dist{y}{z}$. 
	\end{enumerate}
Eine Menge $X$ mit einem Abstand $\mathrm{dist}$ heißt \emph{metrischer Raum $(X,\mathrm{dist})$}. 
\end{defi}
%-----------------------------------------------------------------------------
\begin{aufgabe}
Die vierte Eigenschaft in der Definition eines Abstands heißt Dreiecksungleichung. Überlege dir anhand eines Bildes warum dieser Name gerechtfertigt ist. 
\end{aufgabe}
\begin{aufgabe}
Zeige, dass die erste Forderung (positive Definitheit), d.h $\dist{x}{y}\geq 0$ für alle $x,y\in X$, eine Konsequenz der anderen ist. 
\end{aufgabe}
%-----------------------------------------------------------------------------
Mit diesen Vorbereitungen können wir nun allgemein eine Kugel als Menge aller Punkte, die einen gewissen Abstand $r$ zu einem Mittelpunkt haben beschreiben. 
\begin{defi}
Sei $(X,\mathrm{dist})$ ein metrischer Raum. Für $r>0$ und $x\in X$ heißt die Menge
\[
B(x,r) = \{y\in X: \dist{x}{y}<r\}
\]
die \emph{offene Kugel mit Radius $r$ um $x$}. 
\end{defi}
Nach diesem etwas abstrakteren Einstieg kommen wir nun zu konkreten Beispielen und klären in diesem Rahmen die zweite Frage, ob Kugeln immer \glqq rund\grqq{} sein müssen. \\

\textbf{Müssen Kugeln immer \glqq rund\grqq{} sein? } Die Antwort ist \emph{Nein} und ich hoffe, dass damit euer Weltbild nicht allzu sehr aus den Bahnen geraten wird. Wie ihr euch vermutlich schon gedacht habt, werden wir in diesem Kapitel sehen, dass die Form der Kugel vom gewähltem Abstandsbegriff abhängt. 
\begin{beispiel}
\textit{Die Standardkugeln in $\R^3, \R^2$}\\
Aus der Schule kennt ihr bereits den klassischen (auch Euklidisch genannt) Abstandsbegriff im $\R^3$. Für zwei Vektoren $\vec{u}=(u_1,u_2,u_3)$ und $ \vec{v}=(v_1,v_2,v_3)$ ist dieser gegeben durch 
\[
d_{\mathrm{eukl}}(\vec{u}, \vec{v})=\sqrt{(u_1-v_1)^2+(u_2-v_2)^2 + (u_3-v_3)^2}= \sqrt{\sum_{i=1}^{3}(u_i-v_i)^2}. 
\]
In dem wir $u_3=v_3=0$ setzen erhalten wir den klassischen Abstandsbegriff in der Ebene $\R^2$. 
\begin{aufgabe}
	Überlege dir, dass der Euklidische Abstandsbegriff $d_{\mathrm{eukl}}$ auch ein Abstand im Sinne von obiger Definition ist. 
\end{aufgabe}
Mit diesen Vorbereitungen erhalten wir die offenen Standardkugeln im $\R^3$ und $\R^2$ mit Mittelpunkt im Ursprung, wobei letztere auch gerne einfach nur Kreis genannt wird \smiley.
\begin{figure}[ht]
	\begin{center}
	\begin{tikzpicture}%open ball
	\shade[ball color = gray!40, opacity = 0.4] (0,0) circle (2cm);
	\draw [dashed](0,0) circle (2cm);
	\draw [dashed](-2,0) arc (180:360:2 and 0.6);
	\draw[dashed] (2,0) arc (0:180:2 and 0.6);
	\fill[fill=black] (0,0) circle (1pt);
	\draw[dashed] (0,0 ) -- node[above]{$r$} (2,0);
	\draw (0,0) node[above]{$\vec{0}$};
	%circle
	%\shade[ball color = gray!40, opacity = 0.4] (6,0) circle (2cm);
	\fill[color = gray!40, opacity = 0.4] (6,0) circle (2cm);]
	\draw [dashed](6,0) circle (2cm);
	\fill[fill=black] (6,0) circle (1pt);
	\draw[dashed] (6,0 ) -- node[above]{$r$} (8,0);
	\draw (6,0) node[above]{$\vec{0}$};
	\end{tikzpicture}
	\caption{Standardkugeln in $\R^3$ und $\R^2$}
	\end{center}
\end{figure}
\end{beispiel}
%----------------------------------------------------------------------------
\begin{beispiel}
\textit{Erste nichttriviale Kugeln in der Ebene}\\
Der Euklidische Abstand im $\R^2$ war gegeben durch 
\[
d_{\mathrm{eukl}}(\vec{u}, \vec{v})=\sqrt[\textcolor{red}{2}]{(u_1-v_1)^{\textcolor{red}{2}}+(u_2-v_2)^{\textcolor{red}{2}}}.
\]
 Es lässt sich zeigen, dass man als Exponent statt zwei auch eine Zahl $p\in[1,\infty)$ einsetzen kann und immer noch einen Abstand im Sinne obiger Definiton erhält. Wir definieren also den $p$-Abstand zwischen $\vec{x}=(x_1,x_2)$ und $\vec{y}=(y_1,y_2)$ durch
\[
d_p(\vec{x}, \vec{y})=\sqrt[p]{(x_1-y_1)^p+(x_2-y_2)^p}=\bigg((x_1-y_1)^p+(x_2-y_2)^p\bigg)^{\slfrac{1}{p}}.
\]
Weiterhin erhält man einen Abstandsbegriff durch
\[
d_\infty(\vec{x}, \vec{y}) = \max \left\{|x_1-y_1|, |x_2-y_2|\right\}=\max_{i=1,2} |x_i-y_i |.
\]
\begin{bem}
	Für $p=1$ wird der zugehörige $1$-Abstand auch Taxi-Metrik oder Manhattan-Metrik genannt. Woher könnte dieser Name stammen?
\end{bem}
\begin{aufgabe}
Zeige, dass $d_ \infty$ und $d_1$ Abstandsbegriffe im Sinne obiger Definition darstellen. Für die Dreiecksungleichung von $d_1$ verwende die Dreiecksungleichung für den Absolutbetrag. 
\end{aufgabe}
\begin{bem}
	Um zu zeigen, dass $d_p$ auch für $p\in(1,\infty)$ ein Abstandbegriff ist, benötigt man die sogenannte Höldersche Ungleichung. Dies sprengt aber den Rahmen des Zirkels.
\end{bem}
Wir wollen uns nun gemeinsam überlegen wie die offene Kugel mit Radius $r=1$ und Mittelpunkt $\vec{x}=\vec{0}=(0,0)$ bzgl. des Abstands $d_\infty$ aussieht, also
\[
B_\infty(\vec{0}, 1)= \left\{\vec{y}\in \R^2: d_\infty (\vec{0}, \vec{y})<1\right\}. 
\]
Das bedeutet, dass wir gerade alle Punkte $\vec{y}\in\R^2$ suchen für welche 
\[
\max\{\abs{y_1}, \abs{y_2}\}<1
\]
gilt. Klarerweise ist der Ursprung als Mittelpunkt in der Kugel enthalten. Weiterhin sind die waagrechten bzw. senkrechten Abschnitte von Geraden die durch 
\[
A= \{\vec{y}=(a,0): a\in (-1,1)\}\quad\mbox{und}\quad B=\{\vec{y}=(0,b): b\in(-1,1)\}
\]
innerhalb unserer Kugel, denn z.B. der Punkt $(-0.8, 0)\in A$ erfüllt
\[
\max\{\abs{-0.8}, \abs{0}\}=\max\{0.8, 0\}=0.8 <1. 
\]
Aber auch Punkte wie $(0.9,0.9), (-0.99, -0.99)$ oder $(-0.87, 0.99)$ liegen in unserer Kugeln, denn wir haben z.B. 
\[
\max\{\abs{0.9}, \abs{0.9}\}=0.9<1 \quad\mbox{oder}\quad \max\{\abs{-0.87},\abs{0.99}\}=0.99<1. 
\]
Für das Bild unserer Kugel bzgl. $d_\infty$ bedeutet das gerade, dass wir ein offenes Quadrat mit Kantenlängen 2 erhalten, d.h. 
\begin{figure}[ht]
	\begin{center}
		\begin{tikzpicture}
			%\shade[ball color = gray!40, opacity = 0.4] (-2,-2) rectangle (2,2);
			\fill[color = gray!40, opacity = 0.4] (-2,-2) rectangle (2,2);
			\draw [dashed](-2,-2) rectangle (2,2); 
			\fill[fill=black] (0,0) circle (1pt);
			\draw[dashed] (0,0 ) -- node[above]{$1$} (2,0);
		\end{tikzpicture}
		\caption{Die Einheitskugel bzgl. $d_\infty$}
	\end{center}
\end{figure}
\end{beispiel}
%-----------------------------------------------------------------------------
\begin{bem} Alle oben eingeführten Abstandsbegriffe lassen sich einfach auf den $\R^n$ für $n\in\N$ übertragen. Man setzt für $\vec{u}=(u_1,\dots,u_n)$ und $\vec{v}=(v_1,\dots,v_n)$ und $p\in[1,\infty)$
\[
d_p(\vec{u}, \vec{v})=\left(\sum_{i=1}^n (u_i-v_i)^p\right)^{\slfrac{1}{p}}\quad \mbox{und}\quad d_\infty(\vec{u}, \vec{v})=\max_{i=1,\dots,n}\abs{u_i-v_i},
\]
sodass obige Kugeln leicht auf beliebige Dimension verallgemeinert werden können. 
\end{bem}
%-----------------------------------------------------------------------------
\begin{aufgabe}
	Überlege dir wie die Kugel bzgl. $d_1, d_3,d_4$ etc. in $\R^2$ mit Mittelpunkt im Ursprung und Radius $r=1$ aussieht. Gehe dabei wie oben bei der Einheitskugel für $d_\infty$ vor. Einen Teil der Lösung findest du auf der nächsten Seite, also nicht spicken! \smiley. 
\end{aufgabe}
%------------------------------------------------------------------------
Nach diesem Ausflug in die Welt der \glqq eckigen\grqq{} Kugeln widmen wir uns im Rest des Zirkels
höherdimensionalen Standardkugeln, d.h. Kugeln in $\R^n$ bzgl. $d_2$.\\
\begin{figure}[ht]
	\begin{tikzpicture}
	%\shade[ball color = gray!40, opacity = 0.4] (-2,0)-- (0,2)--(2,0)--(0,-2) -- (-2,0);
	\fill[color = gray!40, opacity = 0.4] (-2,0)-- (0,2)--(2,0)--(0,-2) -- (-2,0);
	\draw[dashed ] (-2,0)-- (0,2)--(2,0)--(0,-2) -- (-2,0); 
	\fill[fill=black] (0,0) circle (1pt);
	\draw[dashed] (0,0 ) -- node[above]{$1$} (2,0);
	%circle
	%\shade[ball color = gray!40, opacity = 0.4] (6,0) circle (2cm);
	\fill[color = gray!40, opacity = 0.4] (6,0) circle (2cm);
	\draw [dashed](6,0) circle (2cm);
	\fill[fill=black] (6,0) circle (1pt);
	\draw[dashed] (6,0 ) -- node[above]{$1$} (8,0);
	\end{tikzpicture}
	\centering
	\caption{Die Einheitskugeln bzgl. $d_1$ und $d_2$ im $\R^2$}
\end{figure}

\textbf{Was sind höherdimensionale Kugeln?} Wie du dir bereits gedacht hast verbirgt sich
hinter diesem Begriff nichts anderes als eine Kugel in $\R^n$ für $n\in \N$ bzgl. einem Abstandsbegriff. Wir wollen uns hier auf den Fall $d_2$ beschränken und untersuchen Kugeln der
Form
\[
B(\vec{x},r)=\{\vec{y}\in\R^n: d_2(\vec{x}, \vec{y})<r\}.
\]

Es lässt sich zeigen
%------------------------
\footnote{Dies übersteigt den Rahmen unsere Fähigkeiten und ist üblicherweise eine Aufgaben im dritten Semester. Wer sich trotzdem angesprochen fühlt es zu probieren findet hier eine Anleitung nach Poincaré, aber Achtung man benötigt mehrdimensionale Integralrechnung \smiley. Bekanntlichermaßen gilt für das Gauß Integral
\[
\int_{-\infty}^\infty e^{-t^2}\dx{t}= \sqrt{\pi}. 
\]
Berechne nun das folgende $n$-dimensionale Integral 
\[
\int_{\R^n} e^{-\|\vec{x}\|^2} \ \dx{x}\quad \mbox{mit}\quad\|\vec{x}\|^2=\sum_{i=1}^n \abs{x_i}^2
\]
unter Verwendung des Gauß Integrals, sowie durch Transformation in $n$-dimensionale Kugelkoordinaten um auf die Formel für das $n$-dimensionale Volumen der Kugel zu schließen.
}
, dass das Volumen der $n$-dimensionalen Kugel von der Form
\begin{align*}
\mathrm{vol}_n(B(\vec{x},r))=\frac{\pi^{\slfrac{n}{2}}}{\Gamma\left(\frac{n}{2}+1\right)}
\end{align*}
ist, wobei $\Gamma(z)$ die Eulersche Gammafunktion bezeichnet. Diese Funktion ist eine Art Interpolation der Fakultät, da die Fakultät nur für natürliche Zahlen definiert ist, und hat die Integraldarstellung
\[
\Gamma(z)=\int_0^\infty t^{z-1}e^{-t}\dx{t}\quad \mbox{für}\, z\in(0,\infty).
\]
Man kann die folgenden Eigenschaften zeigen (freiwillige Übung!): 
\[
\Gamma(z+1)=z\,\Gamma(z) \quad\mbox{für}\, z\in(0,\infty) \quad \mbox{und}\quad \Gamma(n+1)=n! \quad\mbox{für}\, n\in\N. 
\]
In der Schule habt ihr bei einfachen Funktionen bereits gelernt, wie man deren Maximum berechnet, nämlich mittels einer Kurvendiskussion. Dies führt uns zur letzte Frage: \\

\textbf{In welcher Dimension hat die Kugel ein maximales Volumen? } Wir wollen die obige Volumenformel als Funktion in der Variablen $n$ auffassen, da die Formel auch für reelle Zahlen $n\in(0,\infty)$ sinnvoll ist. 
Das Ziel ist nun durch Kurvendiskussion herauszufinden an welcher Stelle ein Maximum vorliegt. Leider ist die Rechnung durch das Auftreten der Gamma-Funktion an manchen Stellen etwas komplizierter, sodass wir am Ende auf einen Computer zurückgreifen müssen. 
Wir definieren also
\[
V(n)=\frac{\pi^{\slfrac{n}{2}}}{\Gamma\left(\frac{n}{2}+1\right)} \quad\mbox{für}\,n\in(0,\infty)
\]
und berechnen die Ableitung $V'(n)$ nach der Variablen $n$. Dafür wollen wir die Quotientenregel verwenden und brauchen somit die Ableitung von Zähler und Nenner. 
\begin{aufgabe}
Zeige, dass für die Ableitung von $\pi^{\slfrac{n}{2}}=\exp(\ln(\pi)\slfrac{n}{2})$ die folgende Formel gilt: 
\[
\frac{\mathrm{d}}{\mathrm {dn}}\pi^{\slfrac{n}{2}}=\frac{1}{2}\, \pi^{\slfrac{n}{2}}\,\ln(\pi). 
\]
\end{aufgabe}
Für die Ableitung des Nenners benötigen wir einen Fakt aus der Analysis, nämlich das wir die Ableitung unter dem Integral bilden dürfen. Wir umgehen dies durch die Einführung einer neuen Funktion, der Digamma Funktion. Da wir am Ende sowieso einen Computer verwenden, ist das ausreichend, da sowohl die Gamma als auch Digamma Funktion implementiert sind. Die Digamma Funktion (auch Nullte-Polygamma Funktion genannt) ist definiert durch
\[
\psi^{(0)}(z)=\psi(z)= \frac{\Gamma'(z)}{\Gamma(z)}\quad\mbox{für}\quad z\in(0,\infty). 
\]
Mit diesen Vorbereitungen erhalten wir mit der Quotientenregel die folgende Form für die Ableitung:
\begin{align*}
V'(n)=\frac{\pi^{\slfrac{n}{2}}\ln(\pi)\,\Gamma\left(\frac{n}{2}+1\right)-\pi^{\slfrac{n}{2}}\,\Gamma\left(\frac{n}{2}+1\right)'}{2\left(\Gamma\left(\frac{n}{2}+1\right)\right)^2}
\end{align*}
Klammern wir im Zähler den Ausdruck $\Gamma\left(\frac{n}{2}+1\right)$ aus, so können wir die Digamma Funktion einfügen und erhalten:
\begin{align*}
	V'(n)&=\frac{\Gamma\left(\frac{n}{2}+1\right) 
		\left[
		\pi^{\slfrac{n}{2}}\ln(\pi)\,\Gamma\left(\frac{n}{2}+1\right)-\pi^{\slfrac{n}{2}}\,\psi\left(\frac{n}{2}+1\right)\right]}{2\left(\Gamma\left(\frac{n}{2}+1\right)\right)^2}\\
		&=\frac{
			\pi^{\slfrac{n}{2}}\ln(\pi)\,\Gamma\left(\frac{n}{2}+1\right)-\pi^{\slfrac{n}{2}}\,\psi\left(\frac{n}{2}+1\right)}{2\left(\Gamma\left(\frac{n}{2}+1\right)\right)}\\
\end{align*}
Dies ist kein wirklich schöner Ausdruck deswegen verwenden wir ein Python Programm und nutzen einen numerischen Löser für nichtlineare Gleichungen. Dabei benutzen wir ein iteratives Verfahren um $V'(n)=0$  zu lösen mit Startwert $x_0=7$.  Genauer verwenden wir den Löser \textit{fsolve} in der Python Bibliothek \textit{SciPy}. Um zu überprüfen, dass tatsächlich ein Maximum vorliegt, lassen wir uns den Graphen der Volumenfunktion noch plotten. Mit Hilfe dieses Plots sehen wir dann, dass aus Monotoniegründen tatsächlich ein Extremwert vorliegt. Wir benutzen nachfolgendes Programm:\\
\newpage
\begin{figure}
\begin{python}
import numpy as np
import scipy.optimize
from scipy.special import gamma, polygamma
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt


#derivative of volume function with respect dimension
def f(n):
return np.pi**(n/2) * (np.log(np.pi) - polygamma(0, n/2+1))/(2*gamma(n/2+1))
#volume function
def vol(n):
return np.pi**(n/2) / gamma(n/2+1)

#solving using fsolve
#x_0 start iteration
x_0 = 7
n = scipy.optimize.fsolve(f, x_0)
print(n, vol(n))
#try also different solvers
x1 = scipy.optimize.broyden1(f, x_0)
x2 = scipy.optimize.broyden2(f, x_0)
x3 = scipy.optimize.anderson(f, x_0)
x4 = scipy.optimize.newton_krylov(f, x_0)
print(x1, x2, x3, x4)
#=================================================

y = np.linspace(-1, 15, 100)
plt.plot(y, vol(y), 'green', label='Volumen($n$)')
plt.plot(y, f(y), 'r', label='Ableitung Volumen($n$)')
plt.legend(loc='upper right')
plt.grid()
plt.xlabel('Dimension $n$')
plt.savefig('Plot_volume.png', dpi=300)
plt.show()
\end{python}
\centering
\caption{Programm zur Berechnung des Maximums und Plotten des Graphens}
\end{figure}

\begin{aufgabe} Modifiziere das obige Modellprogramm um eine beliebige nichtlineare Gleichung deiner Wahl zu lösen. Wie wichtig ist die Rolle des Startwerts des iterativen Verfahrens?
\end{aufgabe}

Diese wenigen Zeilen Code liefern für die Dimensionskoordinate des Maximums  (streng genommen ist es zunächst nur ein kritischer Punkt) den Wert 
\[
n_0=5.2569464. 
\]
\begin{bem}
Mit Hilfe von anderen iterativen Gleichungslösern (diese basieren teilweise auf dem aus der Schule bekannten Newton-Verfahren) erhalten wir die Werte
\[
5.256946046537787, \quad  5.256946046537787,\quad 5.256947361891449,\quad  \mbox{und}\quad 5.256935588835972.
\]
Wir sehen, dass unsere numerischen Werte zumindest in den ersten vier Nachkommastellen übereinstimmen. Die kleineren Unterschiede ergeben sich durch die gewählten Algorithmen und Rechenungenauigkeiten. Der Teilbereich der Mathematik, der sich mit solchen computergestützten Verfahren beschäftigt nennt sich Numerik. 
\end{bem}
%-----------------------------------------------------------------------
Um wirklich zu sehen, dass unser berechneter Wert ein Maximum ist, lassen wir uns noch den Graphen der Volumen- sowie deren Ableitungsfunktion plotten. Hierzu verwenden wir die Python Bibliothek \textit{matplotlib}. 
%----------------------------------------------------------------------

\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{Plot_volume.png}
\centering
\end{figure}
Aus der Monotonie des Graphens der Volumenfunktion könnnen wir ablesen, dass am oben berechneten kritischen Wert \(n_0\) tatsächlich ein Maximum vorliegt. Der Wert des maximalen Volumens errechnet sich durch einsetzten in die Volumenfunktion und wir erhalten
\[
V_{\max} = V(n_0)=5.27776802.
\]

\textbf{Fazit:} Wir haben also gesehen, dass sich entgegen aller Intuition das Maximum des Kugelvolumens zwischen Dimension 5 und 6 befindet. Natürlich ist es dabei nicht klar was eine nicht-ganzzahlige Dimension bedeuten soll, aber man kann zumindest sagen, dass das Volumen mit Dimension $n\geq 6$ wieder kleiner wird und für $n\to\infty$ sogar gegen Null konvergiert. 




\end{document}
